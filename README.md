# Racine

🌲 A lightweight command handler and dispatcher for Bukkit.

```java
CommandNode.root("mycommand").executes(ctx -> ctx.getSender().sendMessage("Welcome to my server!"))
    .then(CommandNode.simple("ping").executes(ctx -> ctx.getSender().sendMessage("Pong!")))
    .then(CommandNode.simple("do")
        .then(CommandNode.simple("something").executes(ctx -> ctx.getSender().sendMessage("Did something!"))))
    .then(new CommandNode<>("name", TokenParser.string()).executes(ctx -> ctx.getSender().sendMessage("Hello " + ctx.argument("name", String.class) + ", nice to meet you!")));
```

```sh
/mycommand # -> Welcome to my server!
/mycommand ping # -> Pong!
/mycommand do something # -> Did something!
/mycommand bob # -> Hello bob, nice to meet you!
```

## ✨ Features

- 💻 Awesome developper experience
- ⚡️ Performant, low overhead
- 🏷️ Tab-completion API
- 🔌 Powerful & extendable parser API
- 🧪 Unit tested

## 📦 Get Racine

```kts
// build.gradle.kts

repositories {
    maven(url = "https://codeberg.org/api/packages/PhoenixUHC/maven")
}

dependencies {
    implementation("io.phoenix:racine:0.4")
}
```
