package io.phoenix.racine.parser;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import io.phoenix.racine.util.StringCursor;
import lombok.val;

public class NumberParserTest {
  @Test void parse() {
    val ca = new StringCursor("69 420");
    val a = TokenParser.i32().parse(ca);
    assertEquals(69, a);
    assertEquals(3, ca.getCursor());

    val cb = new StringCursor("6.9");
    val b = TokenParser.i32().parse(cb);
    assertEquals(6, b);
    assertEquals(4, cb.getCursor());

    val cc = new StringCursor("fifty-five");
    val c = TokenParser.i32().parse(cc);
    assertEquals(null, c);

    val cd = new StringCursor("4.2");
    val d = TokenParser.f32().parse(cd);
    assertEquals(4.2f, d);
  }
}
