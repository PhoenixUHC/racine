package io.phoenix.racine.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@AllArgsConstructor
public class StringCursor {
  private final String value;
  @Getter @Setter private int cursor = 0;

  public char readChar() {
    char c;
    try {
      c = this.value.charAt(cursor);
    } catch (StringIndexOutOfBoundsException e) {
      c = '\0';
    }
    this.cursor++;
    return c;
  }

  public int remaining() {
    return this.value.length() - cursor;
  }

  public boolean hasNext() {
    return this.remaining() > 0;
  }
}
