package io.phoenix.racine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import org.bukkit.plugin.java.JavaPlugin;

import io.phoenix.racine.Command.CommandContext;
import io.phoenix.racine.parser.Parser;
import io.phoenix.racine.parser.TokenParser;
import lombok.Data;

/**
 * <pre>
 * CommandNode
 *
 * A single node in the Racine command tree.
 * </pre>
 */
@Data
public class CommandNode<T> {
  private final String name;
  private final Parser<T> parser;

  private final List<CommandNode<?>> children = new ArrayList<>();

  private Function<CommandContext, Collection<String>> suggests = null;
  private Consumer<CommandContext> callback = null;

  public CommandNode(String name, Parser<T> parser) {
    this.name = name;
    this.parser = parser;
  }

  /**
   * <pre>Registers a new child node for this node.</pre>
   */
  public CommandNode<T> then(CommandNode<?> node) {
    this.children.add(node);
    return this;
  }

  /**
   * <pre>Registers a list of suggestions for this node.</pre>
   */
  public CommandNode<T> suggests(Function<CommandContext, Collection<String>> suggests) {
    this.suggests = suggests;
    return this;
  }

  /**
   * <pre>
   * Registers a new callback for when this node is called.
   * If no callback is defined when this node is called, the nearest callable parent node will be called.
   * </pre>
   */
  public CommandNode<T> executes(Consumer<CommandContext> callback) {
    this.callback = callback;
    return this;
  }

  /**
   * <pre>Registers this command tree as a new Bukkit command.</pre>
   */
  public void register(JavaPlugin plugin) {
    plugin.getCommand(this.name).setExecutor(new Command(this));
  }

  /**
   * <pre>
   * Creates a new root node for the Racine command tree.
   * The parser for this node consistently returns `null` and its name will be used when registering the command.
   * </pre>
   */
  public static CommandNode<Void> root(String name) {
    return new CommandNode<>(name, _cmd -> null);
  }

  /**
   * <pre>
   * Creates a new simple node.
   * The parser for this node will only accept an argument that matches the given name. Otherwise, it returns null.
   * </pre>
   */
  public static CommandNode<String> simple(String name) {
    return new CommandNode<>(name, new TokenParser<String>(token -> token.equals(name) ? token : null)).suggests(ctx -> Arrays.asList(name));
  }
}
