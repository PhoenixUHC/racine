package io.phoenix.racine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.util.StringUtil;

import io.phoenix.racine.util.StringCursor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * <pre>
 * Command
 *
 * The Racine command executor and tab completer.
 * </pre>
 */
public class Command implements CommandExecutor, TabCompleter {
  private final CommandNode<?> root;

  @RequiredArgsConstructor
  public static class CommandContext {
    @Getter private final CommandSender sender;
    private final StringCursor command;
    private final List<CommandNode<?>> nodes = new ArrayList<>();
    private final Map<String, Object> arguments = new HashMap<>();

    @SuppressWarnings("unchecked")
    public <T> T argument(String name, Class<T> clazz) {
      val arg = this.arguments.get(name);
      return arg.getClass() == clazz ? (T) arg : null;
    }
  }

  public Command(CommandNode<?> root) {
    this.root = root;
  }

  private CommandNode<?> match(CommandContext ctx, CommandNode<?> parent, int offset) {
    for (val node : parent.getChildren()) {
      ctx.command.setCursor(offset);

      val parsed = node.getParser().parse(ctx.command);
      if (parsed == null)
        continue;

      ctx.nodes.add(node);
      ctx.arguments.put(node.getName(), parsed);
      return node;
    }

    return null;
  }

  private CommandContext walk(CommandSender sender, String command) {
    val ctx = new CommandContext(sender, new StringCursor(command));
    ctx.nodes.add(this.root);

    var current = this.root;
    var offset = 0;

    while (offset < command.length()) {
      val match = this.match(ctx, current, offset);
      if (match == null) return ctx;
      
      current = match;
      offset = ctx.command.getCursor();
    }

    return ctx;
  }

  @Override
  public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] args) {
    val ctx = this.walk(sender, String.join(" ", args));

    for (int i = ctx.nodes.size() - 1; i >= 0; i--) {
      val node = ctx.nodes.get(i);
      val callback = node.getCallback();
      if (callback != null) {
        callback.accept(ctx);
        return true;
      }
    }

    return false;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command command, String alias, String[] args) {
    val cmp = new ArrayList<String>();
    val ctx = this.walk(sender, String.join(" ", args));

    val node = ctx.nodes.get(ctx.nodes.size() - 1);

    for (val child : node.getChildren()) {
      val suggests = child.getSuggests();
      if (suggests == null) continue;

      StringUtil.copyPartialMatches(
        args[args.length - 1],
        suggests.apply(ctx),
        cmp
      );
    }

    return cmp;
  }
}
