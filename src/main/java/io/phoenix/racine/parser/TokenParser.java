package io.phoenix.racine.parser;

import java.util.function.Function;

import io.phoenix.racine.util.StringCursor;
import lombok.RequiredArgsConstructor;
import lombok.val;

/**
 * <pre>
 * TokenParser
 *
 * A Racine parser for single-word arguments.
 * You may implement its behaviour through the `parser` function, ore use one of the built-in parsers.
 * </pre>
 */
@RequiredArgsConstructor
public class TokenParser<T> implements Parser<T> {
  private final Function<String, T> parser;

  private static Boolean parseBoolean(String token) {
    return switch (token) {
      case "true" -> true;
      case "false" -> false;
      default -> null;
    };
  }

  /**
   * <pre>
   * A TokenParser that matches any single-word argument as a {@link String}.
   * </pre>
   */
  public static TokenParser<String> string() {
    return new TokenParser<>(token -> token);
  }

  /**
   * <pre>
   * A TokenParser that matches one of `true` or `false` as a {@link Boolean}.
   * </pre>
   */
  public static TokenParser<Boolean> bool() {
    return new TokenParser<>(TokenParser::parseBoolean);
  }

  /**
   * <pre>
   * A TokenParser for 8-bit integers.
   * </pre>
   */
  public static NumberParser<Byte> i8() {
    return new NumberParser<>(Number::byteValue);
  }

  /**
   * <pre>
   * A TokenParser for 16-bit integers.
   * </pre>
   */
  public static NumberParser<Short> i16() {
    return new NumberParser<>(Number::shortValue);
  }

  /**
   * <pre>
   * A TokenParser for 32-bit integers.
   * </pre>
   */
  public static NumberParser<Integer> i32() {
    return new NumberParser<>(Number::intValue);
  }

  /**
   * <pre>
   * A TokenParser for 64-bit integers.
   * </pre>
   */
  public static NumberParser<Long> i64() {
    return new NumberParser<>(Number::longValue);
  }

  /**
   * <pre>
   * A TokenParser for 32-bit decimals.
   * </pre>
   */
  public static NumberParser<Float> f32() {
    return new NumberParser<>(Number::floatValue);
  }

  /**
   * <pre>
   * A TokenParser for 64-bit decimals.
   * </pre>
   */
  public static NumberParser<Double> f64() {
    return new NumberParser<>(Number::doubleValue);
  }

  /**
   * <pre>
   * Parses the next argument of the given command and returns it.
   * </pre>
   */
  @Override
  public T parse(StringCursor command) {
    val res = new StringBuilder(command.remaining());
    while (command.hasNext()) {
      val current = command.readChar();
      if (current == ' ')
        return this.parser.apply(res.toString());
      res.append(current);
    }
    command.readChar();
    return this.parser.apply(res.toString());
  }
}
