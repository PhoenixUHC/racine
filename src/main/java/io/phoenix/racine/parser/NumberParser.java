package io.phoenix.racine.parser;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.function.Function;

/**
 * <pre>
 * NumberParser
 *
 * A parser for number types.
 * </pre>
 */
public class NumberParser<T extends Number> extends TokenParser<T> {
  public NumberParser(Function<Number, T> parser) {
    super(token -> {
      try {
        return parser.apply(NumberFormat.getInstance(Locale.US).parse(token));
      } catch (ParseException e) {
        return null;
      }
    });
  }
}
