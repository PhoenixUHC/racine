package io.phoenix.racine.parser;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import io.phoenix.racine.util.StringCursor;
import lombok.val;

public class TokenParserTest {
  @Test void string() {
    val ca = new StringCursor("foo bar");
    val a = TokenParser.string().parse(ca);
    assertEquals("foo", a);
    assertEquals(4, ca.getCursor());

    val cb = new StringCursor("baz qux", 4);
    val b = TokenParser.string().parse(cb);
    assertEquals("qux", b);
    assertEquals(8, cb.getCursor());
  }

  @Test void bool() {
    val ca = new StringCursor("true false");
    val a = TokenParser.bool().parse(ca);
    assertEquals(true, a);
    assertEquals(5, ca.getCursor());

    val cb = new StringCursor("true false", 5);
    val b = TokenParser.bool().parse(cb);
    assertEquals(false, b);
    assertEquals(11, cb.getCursor());

    val cc = new StringCursor("not a bool");
    val c = TokenParser.bool().parse(cc);
    assertEquals(null, c);
  }
}
