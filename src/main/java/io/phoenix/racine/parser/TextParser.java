package io.phoenix.racine.parser;

import io.phoenix.racine.util.StringCursor;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.val;

/**
 * <pre>
 * TextParser
 *
 * A parser for multiple-words arguments, separated by a delimiter (such as "" or '').
 * </pre>
 */
@NoArgsConstructor
@AllArgsConstructor
public class TextParser implements Parser<String> {
  private char delimiter = '"';

  @Override
  public String parse(StringCursor command) {
    if (command.readChar() != this.delimiter) return null;
    val res = new StringBuilder(command.remaining());
    while (command.hasNext()) {
      val current = command.readChar();
      if (current == this.delimiter) {
        command.readChar();
        return res.toString();
      }
      res.append(current);
    }
    return null;
  }
}
