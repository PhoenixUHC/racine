plugins {
  `java-library`
  `maven-publish`
}

group = "io.phoenix"
version = "0.4"

repositories {
  mavenCentral()
  maven(url = "https://hub.spigotmc.org/nexus/content/repositories/public/")
}

dependencies {
  compileOnly("org.spigotmc:spigot-api:1.8.8-R0.1-SNAPSHOT")

  compileOnly("org.projectlombok:lombok:1.18.28")
  annotationProcessor("org.projectlombok:lombok:1.18.28")
  testAnnotationProcessor("org.projectlombok:lombok:1.18.28")

  testImplementation("org.junit.jupiter:junit-jupiter:5.+")
  testRuntimeOnly("org.junit.platform:junit-platform-launcher")
  testImplementation("org.mockito:mockito-core:5.+")
}

configurations {
  arrayOf(compileOnly, implementation).forEach { testImplementation.get().extendsFrom(it.get()) }
}

tasks.test {
  useJUnitPlatform()
  testLogging {
    events("passed", "skipped", "failed")
  }
}

publishing {
  publications {
    create<MavenPublication>("maven") {
      from(components["java"])
    }

    repositories {
      maven {
        name = "Codeberg"
        url = uri("https://codeberg.org/api/packages/PhoenixUHC/maven")
        credentials(HttpHeaderCredentials::class) {
          name = "Authorization"
          value = "token ${project.findProperty("codebergToken")}"
        }
        authentication {
          create("header", HttpHeaderAuthentication::class.java)
        }
      }
    }
  }
}
