package io.phoenix.racine.parser;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import io.phoenix.racine.util.StringCursor;
import lombok.val;

public class TextParserTest {
  @Test void parse() {
    val ca = new StringCursor("\"foo bar\"");
    val a = new TextParser().parse(ca);
    assertEquals("foo bar", a);
    assertEquals(10, ca.getCursor());

    val cb = new StringCursor("\"unfinished");
    val b = new TextParser().parse(cb);
    assertEquals(null, b);

    val cc = new StringCursor("what");
    val c = new TextParser().parse(cc);
    assertEquals(null, c);

    val cd = new StringCursor("\"foo bar\" \"baz qux\"", 0);
    val d = new TextParser().parse(cd);
    assertEquals("foo bar", d);
    assertEquals(10, cd.getCursor());
  }
}
