package io.phoenix.racine.parser;

import io.phoenix.racine.util.StringCursor;

/**
 * <pre>
 * Parser
 *
 * The Racine command parser.
 * When implementing your own parser, keep in mind:
 * - Your parser implementation takes a {@link StringCursor} instance, you'll need to consume characters from the command byte after byte.
 * - After parsing, the given {@link StringCursor} cursor will be expected to point to the next arguments first character location.
 * </pre>
 */
@FunctionalInterface
public interface Parser<T> {
  T parse(StringCursor command);
}
