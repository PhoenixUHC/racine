package io.phoenix.racine;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.ArgumentMatchers.eq;

import org.bukkit.command.CommandSender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.phoenix.racine.parser.TextParser;
import io.phoenix.racine.parser.TokenParser;
import lombok.val;

public class CommandTest {
  CommandNode<?> node;
  Command cmd;

  @BeforeEach void setUp() {
    this.node = CommandNode.root("mycommand").executes(ctx -> ctx.getSender().sendMessage("Welcome to my server!"))
      .then(CommandNode.simple("ping").executes(ctx -> ctx.getSender().sendMessage("Pong!")))
      .then(CommandNode.simple("do")
        .then(CommandNode.simple("something").executes(ctx -> ctx.getSender().sendMessage("Did something!"))))
      .then(CommandNode.simple("text")
        .then(new CommandNode<>("message", new TextParser()).executes(ctx -> ctx.getSender().sendMessage("You sent: " + ctx.argument("message", String.class))))
        .then(new CommandNode<>("message", new TextParser('\'')).executes(ctx -> ctx.getSender().sendMessage("You sent (with 's): " + ctx.argument("message", String.class)))))
      .then(new CommandNode<>("name", TokenParser.string()).executes(ctx -> ctx.getSender().sendMessage("Hello " + ctx.argument("name", String.class) + ", nice to meet you!")));
    this.cmd = new Command(node);
  }

  @Test void root() {
    val s = mock(CommandSender.class);
    val c = mock(org.bukkit.command.Command.class);

    cmd.onCommand(s, c, "", new String[] {});
    verify(s, times(1)).sendMessage(eq("Welcome to my server!"));
  }

  @Test void ping() {
    val s = mock(CommandSender.class);
    val c = mock(org.bukkit.command.Command.class);

    cmd.onCommand(s, c, "", new String[] { "ping" });
    verify(s, times(1)).sendMessage(eq("Pong!"));
  }

  @Test void doSomething() {
    val s1 = mock(CommandSender.class);
    val c1 = mock(org.bukkit.command.Command.class);

    cmd.onCommand(s1, c1, "", new String[] { "do", "something" });
    verify(s1, times(1)).sendMessage(eq("Did something!"));

    val s2 = mock(CommandSender.class);
    val c2 = mock(org.bukkit.command.Command.class);

    cmd.onCommand(s2, c2, "", new String[] { "do" });
    verify(s2, times(1)).sendMessage(eq("Welcome to my server!"));
  }

  @Test void name() {
    val s1 = mock(CommandSender.class);
    val c1 = mock(org.bukkit.command.Command.class);

    cmd.onCommand(s1, c1, "", new String[] { "bob" });
    verify(s1, times(1)).sendMessage(eq("Hello bob, nice to meet you!"));

    val s2 = mock(CommandSender.class);
    val c2 = mock(org.bukkit.command.Command.class);

    cmd.onCommand(s2, c2, "", new String[] { "alice" });
    verify(s2, times(1)).sendMessage(eq("Hello alice, nice to meet you!"));
  }

  @Test void text() {
    val s1 = mock(CommandSender.class);
    val c1 = mock(org.bukkit.command.Command.class);

    cmd.onCommand(s1, c1, "", new String[] { "text", "\"Hello,", "World!\"" });
    verify(s1, times(1)).sendMessage(eq("You sent: Hello, World!"));

    val s2 = mock(CommandSender.class);
    val c2 = mock(org.bukkit.command.Command.class);

    cmd.onCommand(s2, c2, "", new String[] { "text", "'Hello,", "World!'" });
    verify(s2, times(1)).sendMessage(eq("You sent (with 's): Hello, World!"));
  }

  @Test void notFound() {
    val s1 = mock(CommandSender.class);
    val c1 = mock(org.bukkit.command.Command.class);

    cmd.onCommand(s1, c1, "", new String[] { "do", "something", "foo" });
    verify(s1, times(1)).sendMessage(eq("Did something!"));

    val s2 = mock(CommandSender.class);
    val c2 = mock(org.bukkit.command.Command.class);

    cmd.onCommand(s2, c2, "", new String[] { "ping", "bar" });
    verify(s2, times(1)).sendMessage(eq("Pong!"));
  }
}
